import { Logger } from '@nestjs/common';
import {
  ETLInterface,
  ETLHelperInterface,
  LinkMessage,
  SourceMessage,
  DeviceMessage,
  CreateMessage,
} from '@sensorbucket/models';
import { TTNHTTPObject } from './payload';

export default class TTNWorker implements ETLInterface {
  // This worker can only process uplink messages for the source, thethingsnetwork
  private readonly capabilities = ['uplink.source.ttn'];

  private logger: Logger;
  private helper: ETLHelperInterface;

  /**
   *
   * @param helper
   * @param logger
   */
  initialize(helper: ETLHelperInterface, logger: Logger): string[] {
    this.helper = helper;
    this.logger = logger;
    this.logger.log('ETL Script initialized');

    return this.capabilities;
  }

  /**
   * Fired when a new message for this worker has been received
   * @param message The received message
   */
  onMessage(message: LinkMessage) {
    if (message instanceof SourceMessage) {
      return this.parseSourceMessage(message);
    }
    this.logger.warn(`Ignored message with ID: ${message.id}. Not supported.`);
  }

  /**
   * Parses the uplink message
   * extracts the device-data
   * @param srcMessage The uplink message
   */
  async parseSourceMessage(srcMessage: SourceMessage) {
    const payload = JSON.parse(
      Buffer.from(srcMessage.payload, 'base64').toString(),
    ) as HTTPPayload;
    const { body } = payload;

    // Extract useful information
    const deviceEUI = body.hardware_serial;
    const deviceData = body.payload_raw; // Is already base64 encoded
    const dateTime = body.metadata.time;

    // Find corresponding device
    const device = await this.helper.getDevice({
      owner: 0,
      source: {
        devEUI: deviceEUI,
      },
    });

    // Create device message
    const deviceMessage = await CreateMessage(DeviceMessage)
      .from(srcMessage)
      .build({ payload: deviceData, deviceId: device.id });

    // Send uplink object
    this.helper.publish(deviceMessage);
  }
}

interface HTTPPayload {
  body: TTNHTTPObject;
  headers: Record<string, string>;
}
