//
// Auto generated interface for TTN HTTP Integration
// Thanks to: http://json2ts.com/
// Manually changed type "Date" to "string", because we use ISO8601 format
//

export interface PayloadFields {}

export interface Gateway {
  gtw_id: string;
  timestamp: number;
  time: string;
  channel: number;
  rssi: number;
  snr: number;
  rf_chain: number;
  latitude: number;
  longitude: number;
  altitude: number;
}

export interface Metadata {
  time: string;
  frequency: number;
  modulation: string;
  data_rate: string;
  bit_rate: number;
  coding_rate: string;
  gateways: Gateway[];
  latitude: number;
  longitude: number;
  altitude: number;
}

export interface TTNHTTPObject {
  app_id: string;
  dev_id: string;
  hardware_serial: string;
  port: number;
  counter: number;
  is_retry: boolean;
  confirmed: boolean;
  payload_raw: string;
  payload_fields: PayloadFields;
  metadata: Metadata;
  downlink_url: string;
}
